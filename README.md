phash [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](http://godoc.org/gitlab.com/opennota/phash?status.svg)](http://godoc.org/gitlab.com/opennota/phash) [![Pipeline status](https://gitlab.com/opennota/phash/badges/master/pipeline.svg)](https://gitlab.com/opennota/phash/commits/master)
=====

phash is a simple Go wrapper around the [pHash](http://www.phash.org) library.

# Install

    go get -u gitlab.com/opennota/phash
